import numpy as np
import pandas as pd
from os import path
from sklearn.model_selection import train_test_split

def load_data():
    data = np.load("all.npz")
    x = data["x"]
    y = data["y"]
    (X_train, X_test), (Y_train, Y_test) = train_test_split(x,y,test_size=0.2,random_state=42)
    return (X_train, Y_train), (X_test, Y_test)

def face_rec_preprocess(xs):
    # subtract the mean value of each image
    xs -= np.mean(xs,axis=-1) # mean value of each image
    xs = xs * (100 / np.linalg.norm(xs,axis=-1))
    xs -= np.mean(xs,axis=0)
    xs = xs / np.std(xs,axis=0)
    return xs

def load_data_svm():
    load_data()


def create_npz(data_dir="face-rec"):
    df = pd.read_csv(path.join(data_dir,"train.csv"))
    pixels = np.array([[int(token) for token in value.split()] for value in df.pixels.values])
    labels = df.emotion.values
    np.savez_compressed('all.npz',x=pixels,y=labels)

