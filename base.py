import numpy as np
from sklearn.decomposition import PCA
from tqdm import tqdm
from functools import reduce
import matplotlib.pyplot as plt
from keras.datasets import mnist
from keras.utils import to_categorical
from torch.nn import *
from torch import optim
from torch import nn
from utils import create_loader, to_n, to_t, get_default_device
import torch


def hinge_squared_loss(predicted: torch.Tensor, outputs: torch.Tensor):
    outputs = outputs.long()
    hinge_loss = torch.sum(torch.max(torch.zeros_like(
        outputs), 1 - outputs * predicted) ** 2)
    return hinge_loss


class Regularizer:
    ''' if power = 1, it is l1 regularization. if power = 2, it is l2 regularization '''
    def __init__(self, alpha, power):
        self.alpha = alpha
        self.power = power

    def regularize(self, model):
        return self.alpha * torch.mean((torch.abs(model.output_layer.weight) ** self.power))


class BaseModel(nn.Module):
    def __init__(self, network, output_layer, loss_fn):
        super().__init__()
        self.network = network
        self.output_layer = output_layer
        self.to(get_default_device())
        self.loss_fn = loss_fn
        self.metrics = []
        self.regularizers = []
        self.opt = None

    def forward(self, inputs):
        features = self.network(inputs)
        flat_features = features.view((features.shape[0], -1))
        return self.output_layer(flat_features)

    def add_metric(self, metric_name, metric_fn):
        self.metrics.append((metric_name, metric_fn))

    def add_regularizers(self, regularizer: Regularizer):
        self.regularizers.append(regularizer)

    def predict(self, inputs):
        with torch.no_grad():
            return self.forward(inputs)

    def set_opt(self, opt):
        self.opt = opt

    def calculate_loss(self, scores, outputs):
        loss = self.loss_fn(scores, outputs)
        for regularizer in self.regularizers:
            loss = loss + regularizer.regularize(self)
        return loss

    def training_step(self, inputs, outputs):
        assert self.opt is not None, "Please set an optimizer by doing model.set_opt(optim.Adam(model.parameters()))"
        self.train()
        self.opt.zero_grad()
        scores = self.forward(inputs)
        loss = self.calculate_loss(scores, outputs)
        loss.backward()
        self.opt.step()

        return self.calculate_metrics(scores, outputs, loss.item(), train=True)

    def epoch_iteration(self, i, data_loader, train=True):
        results_dict = []
        if train:
            for x, y in tqdm(data_loader, desc=f"fitting epoch {i}", leave=False):
                result_dict = self.training_step(x, y)
                results_dict.append(result_dict)
        else:
            for x, y in tqdm(data_loader, desc=f"evaluating epoch {i}", leave=False):
                result_dict = self.eval_step(x, y)
                results_dict.append(result_dict)

        metrics_dict = {}
        for key in results_dict[0]:
            metrics_dict[key] = np.mean(
                [result_dict[key] for result_dict in results_dict])

        return metrics_dict

    def calculate_metrics(self, scores, outputs, loss=None, train=True):
        with torch.no_grad():
            prefix = "train_" if train else "test_"
            metrics_dict = {f"{prefix}{metric_name}": metric_fn(
                scores, outputs) for metric_name, metric_fn in self.metrics}
            if loss is None:
                loss = self.calculate_loss(scores, outputs).item()
            metrics_dict[f"{prefix}loss"] = loss

            return metrics_dict

    def eval_step(self, inputs, outputs):
        self.eval()
        with torch.no_grad():
            scores = self.forward(inputs)
            return self.calculate_metrics(scores, outputs, train=False)


def create_default_network():
    return Sequential(
        Linear(70, 512),
        ReLU(),
        Linear(512, 512),
        ReLU(),
    )


def create_default_output_layer():
    return Linear(512, 10)


def pprint_metric(i, metrics_dict):
    metrics_info = ' | '.join(
        [f"{metric_name} : {metric_value:.5f}" for metric_name, metric_value in metrics_dict.items()])
    print(f"Epoch {i:4d} | {metrics_info}")


def create_dsvm(network=create_default_network(), output_layer=create_default_output_layer(), loss_fn=hinge_squared_loss):
    def dsvm_acc(predicted, outputs):
        predicted_classes = torch.argmax(predicted, axis=-1)
        outputs_classes = torch.argmax(outputs, axis=-1)
        return sum(predicted_classes == outputs_classes).item() / predicted_classes.shape[0]
    dsvm_accuracy = (
        "acc", dsvm_acc
    )
    model = BaseModel(network, output_layer, loss_fn)
    model.add_metric(*dsvm_accuracy)
    model.set_opt(optim.Adam(model.parameters()))
    return model


def create_dsoftmax(network=create_default_network(), output_layer=create_default_output_layer()):
    def softmax_loss(predicted, outputs):
        return nn.CrossEntropyLoss()(predicted, outputs.long())

    def dsoftmax_acc(predicted, outputs_classes):
        predicted_classes = torch.argmax(predicted, axis=-1)
        return sum(predicted_classes == outputs_classes).item() / predicted_classes.shape[0]
    dsoftmax_accuracy = (
        "acc", dsoftmax_acc
    )
    model = BaseModel(network, output_layer, softmax_loss)
    model.add_metric(*dsoftmax_accuracy)
    model.set_opt(optim.Adam(model.parameters()))
    return model
