import numpy as np
from torch.utils.data import TensorDataset
from torch.utils.data import DataLoader
import torch
def get_default_device() -> torch.device:
    # return torch.device('cpu')
    if torch.cuda.is_available():
        return torch.device('cuda')
    else:
        return torch.device('cpu')


def to_t(data, device=get_default_device()) -> torch.Tensor:

    if isinstance(data, np.ndarray):
        return torch.Tensor(data).to(device)
    elif isinstance(data, torch.Tensor):
        return data.to(device)
    assert False, "used to_tensor on something that was not np.array nor torch.Tensor"

def to_n(data : torch.Tensor) -> np.ndarray:
    return data.cpu().detach().numpy()


def create_loader(xs : np.ndarray,ys : np.ndarray,batch_size=16) -> DataLoader:
    dataset = TensorDataset(to_t(xs),to_t(ys))
    return DataLoader(dataset,batch_size)
