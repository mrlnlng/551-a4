from torch.nn import *
from keras.datasets import cifar10
from base import BaseModel, hinge_squared_loss, create_dsvm, create_dsoftmax, create_loader, Regularizer, pprint_metric
from torch.utils.data import DataLoader
from torch import optim
import numpy as np
from keras.utils import to_categorical
INPUT_SHAPE = (3, 32, 32)
NUM_CLASSES = 10
EPOCHS = 25


def channels_first(data):
    return np.moveaxis(data, -1, 1)

def do_epoch(i,model,train_data_loader,test_data_loader):
    train_metric = model.epoch_iteration(i, train_data_loader, train=True)
    test_metric = model.epoch_iteration(i, test_data_loader, train=False)
    metric = {**train_metric, **test_metric}
    return metric


def load_data():
    (X_train, Y_train), (X_test, Y_test) = cifar10.load_data()
    X_train = channels_first(X_train)
    X_test = channels_first(X_test)
    Y_train = Y_train.reshape((-1)) 
    Y_test = Y_test.reshape((-1)) 
    X_train = X_train.astype('float32') / 255
    X_test = X_test.astype('float32') / 255
    return (X_train, Y_train), (X_test, Y_test)


def to_svm_outputs(labels):
    return to_categorical(labels) * 2 - 1


def load_data_svm():
    (X_train, Y_train), (X_test, Y_test) = load_data()
    Y_train = to_svm_outputs(Y_train)
    Y_test = to_svm_outputs(Y_test)
    return (X_train, Y_train), (X_test, Y_test)


def create_cifar_network(kernel_size=(5, 5), input_shape=INPUT_SHAPE):
    def convblock(in_filters, filters, kernel_size=kernel_size, padding=1, activation=ReLU()):
        return Conv2d(in_filters, filters, kernel_size, padding=padding), activation

    def pool(pool_size=(2, 2),padding=(1,1)):
        return MaxPool2d(pool_size,padding=padding)

    output_nodes = 64 * 8 * 8
    model = Sequential(
        *convblock(3, 32),
        # 32x (32,32)
        pool(),
        # 32x (16,16)
        *convblock(32, 64),
        pool(),
        Flatten(),
        Linear(output_nodes,3072),
        ReLU(),
        Dropout(0.2),
    )
    return model, 3072


def create_output_layer(input_nodes, num_classes=NUM_CLASSES):
    return Linear(input_nodes, num_classes)


def create_dsvm_cifar():
    network, latent_nodes = create_cifar_network()
    output_layer = create_output_layer(latent_nodes)
    return create_dsvm(network=network, output_layer=output_layer)


def create_dsoftmax_cifar():
    network, latent_nodes = create_cifar_network()
    output_layer = create_output_layer(latent_nodes)
    return create_dsoftmax(network=network, output_layer=output_layer)


def fit(model: BaseModel, train_data_loader: DataLoader, test_data_loader: DataLoader, epochs=1):
    metrics = []
    model.set_opt(optim.Adam(model.parameters()))
    model.add_regularizers(Regularizer(0.001, 2))
    for i in range(epochs):
        metric = do_epoch(i, model, train_data_loader, test_data_loader)
        metrics.append(metric)
        pprint_metric(i, metric)

    return metrics


def test_softmax(epochs=EPOCHS):
    model = create_dsoftmax_cifar()
    (X_train, Y_train), (X_test, Y_test) = load_data()
    train_data_loader = create_loader(X_train, Y_train)
    test_data_loader = create_loader(X_test, Y_test)

    metrics = fit(model, train_data_loader, test_data_loader, epochs=epochs)
    return metrics


def test_svm(epochs=EPOCHS):
    model = create_dsvm_cifar()
    (X_train, Y_train), (X_test, Y_test) = load_data_svm()
    train_data_loader = create_loader(X_train, Y_train)
    test_data_loader = create_loader(X_test, Y_test)

    metrics = fit(model, train_data_loader, test_data_loader, epochs=epochs)
    return metrics


if __name__ == "__main__":
    print("Testing deep-softmax")
    print("================")
    softmax_metrics = test_softmax()
    print("Testing deep-svm")
    print("================")
    svm_metrics = test_svm()
    print(softmax_metrics)
    print(svm_metrics)
