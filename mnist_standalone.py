import numpy as np
from sklearn.decomposition import PCA
from tqdm import tqdm
from functools import reduce
import matplotlib.pyplot as plt
from keras.datasets import mnist
from keras.utils import to_categorical
from torch.nn import *
from torch import optim
from torch import nn
from torch.utils.data import DataLoader
from utils import create_loader, to_n, to_t, get_default_device
import torch

from base import create_dsoftmax, create_dsvm, Regularizer, pprint_metric, BaseModel

TRANSFORMS = [
    lambda xs, _: xs.astype('float32') / 255,
    lambda xs, _: xs.reshape((-1, 784))
]

def load_xs(transforms=TRANSFORMS):
    (X_train, _), (X_test, _) = mnist.load_data()
    for transform in transforms:
        X_train = transform(X_train, True)
        X_test = transform(X_test, False)
    return X_train, X_test


def noisy(xs, stddev=1):
    total_elems = reduce(lambda acc, el: acc * el, xs.shape)
    noise = np.random.randn(total_elems).reshape(xs.shape) * stddev
    return xs + noise


def load_xs_pca():
    pca = PCA(n_components=70)
    transforms = TRANSFORMS + [
        lambda xs, is_train: pca.fit_transform(
            xs) if is_train else pca.transform(xs),
        lambda xs, _: noisy(xs)
    ]
    return load_xs(transforms)


def load_ys(one_hot=True):
    (_, Y_train), (_, Y_test) = mnist.load_data()
    if not one_hot:
        return Y_train, Y_test
    Y_train = to_categorical(Y_train)
    Y_test = to_categorical(Y_test)

    return Y_train, Y_test


def load_data(one_hot=True):
    X_train, X_test = load_xs_pca()
    Y_train, Y_test = load_ys(one_hot=False)
    return (X_train, Y_train), (X_test, Y_test)


def load_data_svm():
    X_train, X_test = load_xs_pca()
    Y_train, Y_test = load_ys(one_hot=True)
    Y_train = Y_train * 2 - 1  # to rescale from -1 to 1
    Y_test = Y_test * 2 - 1  # same as above
    return (X_train, Y_train), (X_test, Y_test)


def do_epoch(i,model,train_data_loader,test_data_loader):
    train_metric = model.epoch_iteration(i, train_data_loader, train=True)
    test_metric = model.epoch_iteration(i, test_data_loader, train=False)
    metric = {**train_metric, **test_metric}
    return metric

def fit(model : BaseModel, train_data_loader : DataLoader, test_data_loader : DataLoader, epochs=1):
    # TODO : add linear decay
    metrics = []
    model.add_regularizers(Regularizer(0.001, 2))
    for i in range(epochs):
        metric = do_epoch(i,model, train_data_loader, test_data_loader)
        metrics.append(metric)
        pprint_metric(i, metric)

    return metrics

def fit_decay(model : BaseModel,train_data_loader : DataLoader,test_data_loader : DataLoader,epochs=1):
    initial_lr = lr = 0.1
    initial_noise = noise = 1
    model.set_opt(optim.SGD(model.parameters(), lr=lr,momentum=0.9))
    for i in range(epochs):
        metric = do_epoch(i, train_data_loader, test_data_loader)
        metrics.append(metric)
        pprint_metric(i, metric)

        lr -= initial_lr / epochs
        noise -= initial_noise / epochs

        model.set_opt(optim.SGD(model.parameters(), lr=lr,momentum=0.9))
    
    


EPOCHS = 5


def test_softmax(epochs=EPOCHS):
    model = create_dsoftmax()
    (X_train, Y_train), (X_test, Y_test) = load_data()
    train_data_loader = create_loader(X_train, Y_train)
    test_data_loader = create_loader(X_test, Y_test)

    metrics = fit(model, train_data_loader, test_data_loader, epochs=epochs)
    return metrics


def test_svm(epochs=EPOCHS):
    model = create_dsvm()
    (X_train, Y_train), (X_test, Y_test) = load_data_svm()
    train_data_loader = create_loader(X_train, Y_train)
    test_data_loader = create_loader(X_test, Y_test)

    metrics = fit(model, train_data_loader, test_data_loader, epochs=epochs)
    return metrics


if __name__ == "__main__":
    print("Testing deep-softmax")
    print("================")
    softmax_metrics = test_softmax()
    print("Testing deep-svm")
    print("================")
    svm_metrics = test_svm()
    print(softmax_metrics)
    print(svm_metrics)
